# ghc-bootstrap

#### 介绍

Glorious Glasgow Haskell Compiler 的二进制分发包

#### 软件架构

这是用来 bootstrap GHC 的 GHC 二进制软件包。

本软件包目前仅提供 X86_64 和 AArch64 的支持，二进制程序来自 GHC 官方网站。

请不要在外部使用该软件包，要编译 Haskell 程序，请使用 `ghc` 软件包。
