%global ghc_ver 9.2.3

%ifarch x86_64 aarch64
%define sysname deb10
%endif

Name:           ghc-bootstrap
Version:        %{ghc_ver}
Release:        1
Summary:        Binary distributions of The Glorious Glasgow Haskell Compiler
License:        BSD-3-Clause
URL:            https://downloads.haskell.org/~ghc/%{ghc_ver}
Source0:        LICENSE
# This is GHC-signing-key.asc, for checking the integrity of the tarballs.
Source1:        https://keys.openpgp.org/vks/v1/by-fingerprint/88B57FCF7DB53B4DB3BFA4B1588764FBE22D19C4#/GHC-signing-key.asc
Source2:        https://downloads.haskell.org/~ghc/%{ghc_ver}/ghc-%{ghc_ver}-x86_64-%{sysname}-linux.tar.xz
Source3:        https://downloads.haskell.org/~ghc/%{ghc_ver}/ghc-%{ghc_ver}-x86_64-%{sysname}-linux.tar.xz.sig
Source4:        https://downloads.haskell.org/~ghc/%{ghc_ver}/ghc-%{ghc_ver}-aarch64-%{sysname}-linux.tar.xz
Source5:        https://downloads.haskell.org/~ghc/%{ghc_ver}/ghc-%{ghc_ver}-aarch64-%{sysname}-linux.tar.xz.sig
Source6:        ghc-%{ghc_ver}-x86_64-%{sysname}-linux.tar.xz.aa
Source7:        ghc-%{ghc_ver}-x86_64-%{sysname}-linux.tar.xz.ab
Source8:        ghc-%{ghc_ver}-x86_64-%{sysname}-linux.tar.xz.ac
Source9:        ghc-%{ghc_ver}-aarch64-%{sysname}-linux.tar.xz.aa
Source10:       ghc-%{ghc_ver}-aarch64-%{sysname}-linux.tar.xz.ab
Source11:       ghc-%{ghc_ver}-aarch64-%{sysname}-linux.tar.xz.ac

BuildRequires:  chrpath
BuildRequires:  fdupes
BuildRequires:  gmp-devel
BuildRequires:  libffi
BuildRequires:  libatomic
BuildRequires:  ncurses-libs
BuildRequires:  pkgconfig
BuildRequires:  pkgconfig(libffi)
BuildRequires:  numactl-devel
BuildRequires:  gnupg2
BuildRequires:  tar
BuildRequires:  xz
Requires:       gmp-devel
Requires:       libffi
Requires:       libatomic
Requires:       ncurses-libs
Requires:       pkgconfig(libffi)
Requires:       numactl-devel
Requires:       libffi-devel

Conflicts:      ghc-base
Provides:       ghc-bootstrap-devel
ExclusiveArch:  x86_64 aarch64
AutoReq:        off

%description
This package contains a binary distribution of The Glorious Glasgow
Haskell Compilation System.

The tarballs come from the ghc download page.
This package is only used for bootstraping ghc.

Do not install this package! Install 'ghc' instead.

%prep
cd ../SOURCES
cat %{SOURCE6} %{SOURCE7} %{SOURCE8} > %{SOURCE2}
cat %{SOURCE9} %{SOURCE10} %{SOURCE11} > %{SOURCE4}
cd ../BUILD

cp %{SOURCE0} .
cp %{SOURCE1} .
cp %{SOURCE2} .
cp %{SOURCE3} .
cp %{SOURCE4} .
cp %{SOURCE5} .

# To avoid malicious tampering, checking the integrity of the upstream tarballs.
gpg --import %{SOURCE1}
gpg --verify ghc-%{version}-%{_arch}-%{sysname}-linux.tar.xz.sig ghc-%{version}-%{_arch}-%{sysname}-linux.tar.xz

tar Jxf ghc-%{version}-%{_arch}-%{sysname}-linux.tar.xz

%build

%install
cd ghc-%{version}
./configure --prefix=/opt
%make_install

%post
/opt/bin/ghc-pkg recache

%files
%license LICENSE
/opt/bin/ghc
/opt/bin/ghc-%{version}
/opt/bin/ghci
/opt/bin/ghci-%{version}
/opt/bin/ghc-pkg
/opt/bin/ghc-pkg-%{version}
/opt/bin/haddock
/opt/bin/haddock-ghc-%{version}
/opt/bin/hp2ps
/opt/bin/hpc
/opt/bin/hsc2hs
/opt/bin/runghc
/opt/bin/runghc-%{version}
/opt/bin/runhaskell
/opt/lib/ghc-%{version}/*
/opt/share/doc/ghc-%{version}/*
/opt/share/man/man1/ghc.1

%changelog
* Tue Nov 14 2023 Lin Runze <lrzlin@163.com> - 9.2.3-1
- Initial packaging.
