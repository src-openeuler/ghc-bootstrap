# ghc-bootstrap

#### Description

Binary distributions of The Glorious Glasgow Haskell Compiler

#### Software Architecture

This package is only for GHC bootstraping, do not use it for any other purpose.

This package currently only support X86_64 and AArch64.

For compiling Haskell Program, using the `ghc` package instead.
